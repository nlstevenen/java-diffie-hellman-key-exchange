package com.exceptions;

/**
 * Created by Steven Kok on 29/05/2014.
 */
public class CreatingDHAgreementException extends Exception {
    public CreatingDHAgreementException(String message) {
        super(message);
    }

    public CreatingDHAgreementException(String message, Throwable cause) {
        super(message, cause);
    }

}
