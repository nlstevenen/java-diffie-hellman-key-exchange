package com.frontend;

public enum StatusCode {
    OK(0),
    ERROR(-1);

    private final Integer statusCode;

    StatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public Integer getStatusCode() {
        return statusCode;
    }
}
