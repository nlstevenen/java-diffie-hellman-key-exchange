package com.frontend.models.response.DiffieHellman;

import com.frontend.models.response.Response;

import java.math.BigInteger;
import java.util.UUID;

/**
 * Created by Steven Kok on 5/29/14.
 */
public class DHAgreementResponse implements Response {

    private BigInteger p;
    private BigInteger g; //base
    private byte[] publicA;
    private UUID keyExchangeSessionId;

    public DHAgreementResponse(BigInteger p, BigInteger g, byte[] publicA, UUID keyExchangeSessionId) {
        this.p = p;
        this.g = g;
        this.publicA = publicA;
        this.keyExchangeSessionId = keyExchangeSessionId;

    }

    public DHAgreementResponse() {
    }

    public byte[] getPublicA() {

        return publicA;
    }

    public UUID getKeyExchangeSessionId() {
        return keyExchangeSessionId;
    }

    public BigInteger getP() {

        return p;
    }

    public BigInteger getG() {
        return g;
    }
}
