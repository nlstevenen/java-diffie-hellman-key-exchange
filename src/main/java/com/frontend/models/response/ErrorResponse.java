package com.frontend.models.response;

import com.frontend.StatusCode;

public class ErrorResponse implements Response {
    private String message;
    private final Integer statusCode = StatusCode.ERROR.getStatusCode();

    public String getMessage() {
        return message;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public ErrorResponse() {

    }

    public ErrorResponse(String message) {
        this.message = message;

    }
}
