package com.frontend.models.response;

import com.frontend.StatusCode;

public class DefaultResponse implements Response {
    private final Integer statusCode = StatusCode.OK.getStatusCode();

    public DefaultResponse() {
    }

    public int getStatusCode() {
        return statusCode;

    }

}
