package com.frontend.models.request.DiffieHellman;

import com.frontend.models.request.Request;

import java.util.UUID;

public class FinalizeDHRequest implements Request {

    private UUID keyExchangeSessionId;
    private byte[] publicB;

    public UUID getKeyExchangeSessionId() {
        return keyExchangeSessionId;
    }

    public FinalizeDHRequest(UUID keyExchangeSessionId, byte[] publicB) {
        this.keyExchangeSessionId = keyExchangeSessionId;

        this.publicB = publicB;
    }

    public FinalizeDHRequest() {
    }

    public byte[] getPublicB() {
        return publicB;
    }
}
