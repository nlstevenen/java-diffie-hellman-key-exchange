package com.CryptoAlgorithms.DiffieHellman;

import java.math.BigInteger;

/**
 * Created by Steven Kok on 5/29/14.
 */
public class DHAgreementModel {
    private final BigInteger p; //public prime
    private final BigInteger g; //public base; primitive root modulo p
    private final byte[] a; // private key
    private final byte[] publicA; // public key

    public byte[] getPublicA() {
        return publicA;
    }

    public byte[] get_a() {
        return a;
    }

    public DHAgreementModel(BigInteger p, BigInteger g, byte[] a, byte[] publicA) {
        this.p = p;
        this.g = g;
        this.a = a;
        this.publicA = publicA;
    }

    public BigInteger getP() {

        return p;
    }

    public BigInteger getG() {
        return g;
    }
}
