package controllers;

import com.CryptoAlgorithms.DiffieHellman.DHAgreementModel;
import com.CryptoAlgorithms.DiffieHellman.DHAlgorithm;
import com.frontend.models.request.DiffieHellman.FinalizeDHRequest;
import com.frontend.models.response.DefaultResponse;
import com.frontend.models.response.DiffieHellman.DHAgreementResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("file:src/main/webapp/WEB-INF/mvc-dispatcher-servlet.xml")
public class KeyExchangeControllerTest {

    @Autowired
    private WebApplicationContext webApplicationContext;
    private MockMvc mockMvc;
    private DHAlgorithm dhAlgorithm;
    ObjectMapper objectMapper;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        this.dhAlgorithm = new DHAlgorithm();
        this.objectMapper = new ObjectMapper();
    }

    @Test
    public void shouldContactController() throws Exception {

        mockMvc.perform(get("/keyExchange/initialize").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"));
    }

    @Test
    public void shouldReceiveDHAgreement() throws Exception {
        DHAgreementResponse dhAgreementResponse = initializeDHAgreement();
    }

    private DHAgreementResponse initializeDHAgreement() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/keyExchange/initialize").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DHAgreementResponse.class);
    }

    @Test
    public void shouldFinalizeDHAgreement() throws Exception {
        DHAgreementResponse serverDHResponse = initializeDHAgreement();

        DHAgreementModel localDHAgreementModel = dhAlgorithm.generateInitialNumbers(serverDHResponse.getP(), serverDHResponse.getG());
        FinalizeDHRequest finalizeDHRequest = new FinalizeDHRequest(serverDHResponse.getKeyExchangeSessionId(), localDHAgreementModel.getPublicA());

        DefaultResponse defaultResponse = finalizeDHAgreement(finalizeDHRequest);
    }

    private DefaultResponse finalizeDHAgreement(FinalizeDHRequest finalizeDHRequest) throws Exception {
        String jsonFinalizeDHRequest = objectMapper.writeValueAsString(finalizeDHRequest);

        MvcResult mvcResult = mockMvc.perform(get("/keyExchange/finalize")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(jsonFinalizeDHRequest)
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andReturn();

        return objectMapper.readValue(mvcResult.getResponse().getContentAsString(), DefaultResponse.class);
    }


}